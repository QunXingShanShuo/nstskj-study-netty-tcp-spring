#!/bin/bash

#需要启动的Java主程序（main方法类）
APP_MAINCLASS=com.nstskj.study.netty.tcp.NettySpringApplication


#日志输出路径
export LOG_OUT=/dev/null
#logback配置文件
export logback_file=""
#运行环境
export profile=""

#选择运行环境参数
export mode=""

#得到当前启动脚本的上一级路径  默认启动脚本在主路径的 bin目录下
export SERVIER_BASE_HOME=$(cd "$(dirname "$0")/../"; pwd)

#这里拼接所有依赖的jar包
ALL_JARS="$(find  "${SERVIER_BASE_HOME}"/lib  -name '*.jar' | paste -d : -s)"

#java虚拟机启动参数
export DEFAULT_SEARCH_LOCATIONS="classpath:/,classpath:/config/,file:./,file:./config/"
export CUSTOM_SEARCH_LOCATIONS="${DEFAULT_SEARCH_LOCATIONS},file:${SERVIER_BASE_HOME}/conf/,file:${SERVIER_BASE_HOME}/config/"
#启动参数 最小堆  最大堆  线程栈大小    G1不需要指定年轻代，会自动调整  -Xmn678m
JAVA_OPTS="${JAVA_OPTS} -Xms1024m  -Xmx1024m  -Xss512K  "
JAVA_OPTS="${JAVA_OPTS} -XX:MetaspaceSize=256M -XX:MaxMetaspaceSize=256M "
# 使用 parNew 及 cms 垃圾收集器
JAVA_OPTS="${JAVA_OPTS} -XX:+UseG1GC -XX:MaxGCPauseMillis=20 -XX:InitiatingHeapOccupancyPercent=45 -XX:+ExplicitGCInvokesConcurrent "
JAVA_OPTS="${JAVA_OPTS} -XX:MaxInlineLevel=15 -Djava.awt.headless=true"
# 定义GC日志存放目录
export GC_LOG_DIR="${SERVIER_BASE_HOME}/gcLogs/"
# 判断存放gc日志的目录是否存在 如果不存在则创建目录
if [ ! -d "${GC_LOG_DIR}" ] ; then
   echo "mkdir -p ${GC_LOG_DIR}"
   mkdir -p ${GC_LOG_DIR}
fi
# 配置gc日志文件目录
JAVA_OPTS="${JAVA_OPTS} -Xloggc:${GC_LOG_DIR}gc-%t.log -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M "
# 配置gc日志文件信息
JAVA_OPTS="${JAVA_OPTS} -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation "
#上面用于配置jvm参数 下面为 jar的信息 及 main 的传入参数
#设置启动依赖的jar及主类
JAVA_OPTS="${JAVA_OPTS} -cp ${ALL_JARS}  ${APP_MAINCLASS} "

# 加入spring 启动配置
JAVA_OPTS="${JAVA_OPTS} --spring.config.location=${CUSTOM_SEARCH_LOCATIONS}"

# logback 配置文件
if [ "x${logback_file}" != "x" ]; then
    JAVA_OPTS="${JAVA_OPTS} --logging.config=${logback_file}"
fi

#环境配置
if [ "x${profile}" != "x" ]; then
     JAVA_OPTS="${JAVA_OPTS} --spring.profiles.active=${profile}"
fi
#自定义的扩展参数
JAVA_OPTS="${JAVA_OPTS} ${JAVA_OPT_EXT}"

###################################
#(函数)判断程序是否已启动
#
#说明：
#使用JDK自带的JPS命令及grep命令组合，准确查找pid
#jps 加 l 参数，表示显示java的完整包路径
#使用awk，分割出pid ($1部分)，及Java程序名称($2部分)
###################################
#初始化psid变量（全局）
psid=""
checkpid() {
   javaps=`ps ax | grep "${SERVIER_BASE_HOME}/" | grep java | grep -v grep | awk '{print $1}'`
   if [ -n "$javaps" ]; then
      #psid=`echo $javaps | awk '{print $0}'`
      psid="${javaps}"
   else
      psid=""
   fi
}

##检查mode
checkmod(){
    if [ ${mode}x == "K8S"x ];then
       java ${JAVA_OPTS}
    else
       nohup java ${JAVA_OPTS}  > ${LOG_OUT} 2>&1 &
    fi
}


###################################
#(函数)启动程序
#
#说明：
#1. 首先调用checkpid函数，刷新$psid全局变量
#2. 如果程序已经启动（$psid不等于0），则提示程序已启动
#3. 如果程序没有被启动，则执行启动命令行
#4. 启动命令执行后，再次调用checkpid函数
#5. 如果步骤4的结果能够确认程序的pid,则打印[OK]，否则打印[Failed]
#注意：echo -n 表示打印字符后，不换行
#注意: "nohup 某命令 >/dev/null 2>&1 &" 的用法
###################################
start() {
   checkpid
   if [ -n "$psid" ] ; then
      echo "================================"
      echo "warn: $APP_MAINCLASS already started! (pid=$psid)"
      echo "================================"
   else
      echo -n "Starting ${APP_MAINCLASS} ..."
      # 这里手动配置 配置文件的目录 及 logback 的 配置文件  如果在配置中配置了logback，则会忽略这个配置
      #nohup java ${JAVA_OPTS}  > ${LOG_OUT} 2>&1 &
      checkmod
      # java ${JAVA_OPTS}
      checkpid
      if [ -n "$psid"  ]; then
         echo "(pid=$psid) [OK]"
      else
         echo "[Failed]"
      fi
   fi
}

###################################
#(函数)停止程序
#
#说明：
#1. 首先调用checkpid函数，刷新$psid全局变量
#2. 如果程序已经启动（$psid不等于0），则开始执行停止，否则，提示程序未运行
#3. 使用kill -9 pid命令进行强制杀死进程
#4. 执行kill命令行紧接其后，马上查看上一句命令的返回值: $?
#5. 如果步骤4的结果$?等于0,则打印[OK]，否则打印[Failed]
#6. 为了防止java程序被启动多次，这里增加反复检查进程，反复杀死的处理（递归调用stop）。
#注意：echo -n 表示打印字符后，不换行
#注意: 在shell编程中，"$?" 表示上一句命令或者一个函数的返回值
###################################
stop() {
   checkpid

   if [ -n "$psid" ]; then
      echo -n "Stopping $APP_MAINCLASS ...(pid=$psid) "
      echo "${psid}"  | xargs kill -9
      if [ $? -eq 0 ]; then
         echo "[OK]"
      else
         echo "[Failed]"
      fi

      checkpid
      if [ -n "$psid" ]; then
         stop
      fi
   else
      echo "================================"
      echo "warn: $APP_MAINCLASS is not running"
      echo "================================"
   fi
}

###################################
#(函数)检查程序运行状态
#
#说明：
#1. 首先调用checkpid函数，刷新$psid全局变量
#2. 如果程序已经启动（$psid不等于0），则提示正在运行并表示出pid
#3. 否则，提示程序未运行
###################################
status() {
   checkpid
   if [ $psid -ne 0 ];  then
      echo "$APP_MAINCLASS is running! (pid=$psid)"
   else
      echo "$APP_MAINCLASS is not running"
   fi
}

###################################
#(函数)打印系统环境参数
###################################
info() {
   echo "System Information:"
   echo "****************************"
   echo `head -n 1 /etc/issue`   #查看操作系统版本
   echo `uname -a`
   echo
   echo "JAVA_HOME = $(readlink -nf $(which java) | xargs dirname | xargs dirname | xargs dirname)"
   echo `java -version`
   echo
   echo "SERVIER_BASE_HOME = ${SERVIER_BASE_HOME}"
   echo "APP_MAINCLASS = ${APP_MAINCLASS}"
   echo "ALL_JARS size = `echo ${ALL_JARS} | awk -F ':' '{print NF}'`"
   echo "CONFIG_DIR file list : "
   ls -lh ${SERVIER_BASE_HOME}/config
   echo "****************************"
}

###################################
#读取脚本的第一个参数($1)，进行判断
#参数取值范围：{start|stop|restart|status|info}
#如参数不在指定范围之内，则打印帮助信息
###################################
case "$1" in
   'start')
      start
      ;;
   'stop')
     stop
     ;;
   'restart')
     stop
     start
     ;;
   'status')
     status
     ;;
   'info')
     info
     ;;
  *)
     echo "Usage: $0 {start|stop|restart|status|info}"
     info
     exit 1
esac
exit 0

