package com.nstskj.study.netty.tcp.netty.protocol.constants;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:08
 * @Description 协议命令常量
 */
public interface ProtocolCmdConstants {

    /**
     * 协议数据包最小长度
     * 头2 + 长度 4 +  版本 2 + 命令码 2 + 序列化 4
     */
    int PROTOCOL_MIN_LEN = 14;

    /**
     * 协议起始头
     */
    short PROTOCOL_HEAD = 0X55AA;


}
