package com.nstskj.study.netty.tcp.netty.protocol.handler.end;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 19:39
 * @Description 设备端点数据处理者
 */
public interface DeviceEndpointHandler {

    /**
     * 设备端点数据处理
     *
     * @param abstractSession  会话
     * @param abstractEndpoint 端点对象
     * @param endpointData     端点数据
     */
    void endpointHandler(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData);


}
