package com.nstskj.study.netty.tcp.netty.protocol.executor.end;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 14:12
 * @Description 设备端点数据工作者执行器，用于处理各个端点的信息，
 * 端点信息数据解析及发布端点数据事件
 * 缓存及处理及发布
 */
public interface DeviceEndpointDataWorkHandlerExecutor {

    /**
     * 向指定的sessionId的指定端点地址发送数据
     *
     * @param sessionId  会话id
     * @param address    地址
     * @param endDataStr 端点数据
     * @return
     */
    boolean sendEndpointData(String sessionId, short address, String endDataStr);

    /**
     * 得到近期端点缓存的数据
     *
     * @param sessionId
     * @param address
     * @return
     */
    List<EndpointData> getEndpointDataList(String sessionId, short address);
}
