package com.nstskj.study.netty.tcp.config;

import com.nstskj.study.netty.tcp.common.base.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import java.net.BindException;
import java.util.stream.Collectors;

/**
 * 全局异常处理器
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/14 11:19
 */
@Slf4j
@RestControllerAdvice
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class GlobalExceptionProcessor {

    /**
     * 没有捕获的异常全部在这里处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class})
    public Result exception(Throwable e, HttpServletResponse response) {
        log.error("error class:{}  mes: {}", e.getClass().getName(), e.getMessage(), e);
        return Result.err(e.getMessage());
    }

    /**
     * 输入参数校验不通过
     *
     * @param e
     * @return
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class, ValidationException.class})
    public Result methodArgumentNotValidException(Exception e, HttpServletResponse response) {
        String errorMes = "";
        if (e instanceof MethodArgumentNotValidException) {
            errorMes = ((MethodArgumentNotValidException) e).getBindingResult()
                    .getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(","));
        } else if (e instanceof BindException) {
            errorMes = e.getMessage();
        } else {
            errorMes = e.getMessage();
        }
        log.error(String.format("error class:%s  mes: %s", e.getClass().getName(), e.getMessage()), e);
        //输入参数不合法
        return Result.err(errorMes);
    }


}
