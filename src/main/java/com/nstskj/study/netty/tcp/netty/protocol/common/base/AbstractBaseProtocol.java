package com.nstskj.study.netty.tcp.netty.protocol.common.base;

import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import io.netty.util.AttributeMap;
import io.netty.util.DefaultAttributeMap;
import lombok.Data;

import java.io.Serializable;

/**
 * 抽象协议对象
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 20:51
 */
@Data
public abstract class AbstractBaseProtocol implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性map，并发安全，支持泛型
     *
     * @see AttributeKey#valueOf(java.lang.String)
     * @see AttributeMap#attr(io.netty.util.AttributeKey)
     */
    private DefaultAttributeMap attributeMap;

    /**
     * 协议起始头 2个字节
     */
    private Short head;

    /**
     * 数据域长度 4个字节
     */
    private Integer dataLength;

    /**
     * 版本号 2字节
     */
    private String version;

    /**
     * 命令码
     */
    private Short cmd;

    /**
     * 序列号
     */
    private Integer serial;

    /**
     * 命令数据
     */
    private byte[] cmdData;

    /**
     * 默认构造 ，提供默认值
     */
    public AbstractBaseProtocol() {
        this.attributeMap = new DefaultAttributeMap();
    }

    /**
     * 得到指定属性名对应的属性信息数据
     * 属性key的构建 {@link  AttributeKey#valueOf(java.lang.String)}
     *
     * @param key 属性key
     * @param <T> 值的类型
     * @return 返回属性，属性为一个 key vale类型，可以添加和删除
     */
    public <T> Attribute<T> attr(AttributeKey<T> key) {
        return this.attributeMap.attr(key);
    }

    /**
     * 判断是否存在指定的属性
     *
     * @param key 属性名
     * @param <T> 属性值类型
     * @return true 存在
     */
    public <T> boolean hasAttr(AttributeKey<T> key) {
        return this.attributeMap.hasAttr(key);
    }
}
