/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nstskj.study.netty.tcp.netty.config.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 10:06
 * @Description 线程属性配置
 */
@Slf4j
@Data
public class ThreadExecutionProperties {

    /**
     * 线程的前缀
     */
    private String threadNamePrefix = "thread-";

    /**
     * 队列的大小
     */
    private int queueCapacity = Integer.MAX_VALUE;

    /**
     * 核心线程数
     */
    private int coreSize = 8;

    /**
     * 最大线程数
     */
    private int maxSize = Integer.MAX_VALUE;

    /**
     * 是否允许超时
     */
    private boolean allowCoreThreadTimeout = true;

    /**
     * 保持时间
     */
    private Duration keepAlive = Duration.ofSeconds(60);

}
