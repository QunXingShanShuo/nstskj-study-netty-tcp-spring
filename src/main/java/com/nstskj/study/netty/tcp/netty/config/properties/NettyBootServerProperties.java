package com.nstskj.study.netty.tcp.netty.config.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 17:42
 * @Description netty服务器配置
 */
@Slf4j
@Data
public class NettyBootServerProperties {
    /**
     * netty服务器端口
     */
    private int port = 50050;

    /**
     * 主线程的名称
     */
    private String bossTreadName = "boss-";

    /**
     * 工作线程名称
     */
    private String workThreadName = "word-";
}
