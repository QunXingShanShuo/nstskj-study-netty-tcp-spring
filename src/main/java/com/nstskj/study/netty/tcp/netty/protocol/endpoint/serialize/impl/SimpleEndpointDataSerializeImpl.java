package com.nstskj.study.netty.tcp.netty.protocol.endpoint.serialize.impl;

import cn.hutool.core.util.HexUtil;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.serialize.EndpointDataSerialize;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 21:27
 * @Description 简单的序列化
 */
@Slf4j
@Component
public class SimpleEndpointDataSerializeImpl implements EndpointDataSerialize {

    /**
     * 序列化，把设备上传的数据转换为可识别的字符串
     *
     * @param abstractSession
     * @param abstractEndpoint
     * @param endpointData
     * @return
     */
    @Override
    public void serialize(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData) {
        String hexStr = endpointData.getHexStr();
        byte[] bytes = HexUtil.decodeHex(hexStr);
        int data = ((int) bytes[0] & 0XFF) * 256 + ((int) bytes[1] & 0XFF);
        endpointData.setResultData("" + data);
    }

    /**
     * 反序列化 ，把需要发送的数据转换为对应的命令报文
     *
     * @param abstractSession
     * @param abstractEndpoint
     * @param endpointData
     * @return
     */
    @Override
    public AbstractOutputNetTcpMessage deserialize(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData) {
        return null;
    }
}
