package com.nstskj.study.netty.tcp.netty.constants;

import io.netty.util.AttributeKey;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:23
 * @Description netty用到的一些常量
 */
public interface NettyProtocolConstants {

    /**
     * 管道的session Id
     */
    AttributeKey<String> CHANNEL_SESSION_ID = AttributeKey.valueOf("channel_session_id");

}
