package com.nstskj.study.netty.tcp.netty.protocol.definition;

import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base.AbstractInputNetTcpMessage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:11
 * @Description 协议消息定义
 */
@Data
public class ProtocolMsgBeanDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 命令码
     */
    private short cmd;

    /**
     * 消息的命令数据最小长度
     */
    private int cmdDataMinLen;

    /**
     * 抽象的输入消息的class
     */
    private Class<? extends AbstractInputNetTcpMessage> abstractInputNetTcpMessageClass;

    /**
     * 字段定义
     */
    private List<ProtocolMsgFieldDefinition> fieldDefinitions;

}
