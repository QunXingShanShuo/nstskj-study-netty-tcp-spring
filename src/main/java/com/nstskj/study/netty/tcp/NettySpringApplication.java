package com.nstskj.study.netty.tcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 20:39
 * @Description
 */
@SpringBootApplication
public class NettySpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettySpringApplication.class, args);
    }

}
