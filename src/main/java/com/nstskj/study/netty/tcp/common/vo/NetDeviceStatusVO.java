package com.nstskj.study.netty.tcp.common.vo;

import com.nstskj.study.netty.tcp.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/23 11:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NetDeviceStatusVO extends BaseVO {
    private static final long serialVersionUID = -1L;

    private Boolean status;
}
