package com.nstskj.study.netty.tcp.netty.protocol.session.manage;

import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;

import java.util.Map;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:05
 * @Description 网络设备会话管理器
 */
public interface NetDeviceSessionManage {

    /**
     * 通过sessionid 得到 session对象
     *
     * @param sessionId
     * @return
     */
    AbstractSession getSession(String sessionId);

    /**
     * 得到所以的会话及其对应信息
     *
     * @return
     */
    Map<String, AbstractSession> getSessionMap();
}
