package com.nstskj.study.netty.tcp.netty.protocol.definition.enums;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:13
 * @Description 协议字段类型定义
 */
public enum MsgFieldTypeEnums {
    /**
     * 字符串，直接转换为字符串，对应ascII编码
     */
    STRING_TYPE(-1),
    /**
     * 一个字节
     */
    BYTE_TYPE(1),
    /**
     * 短整型
     */
    SHORT_TYPE(2),
    /**
     * int 4个字节
     */
    INTEGER_TYPE(4),

    /**
     * long 8个字节
     */
    LONG_TYPE(8),

    /**
     * 字节数组
     */
    BYTE_ARRAY_TYPE(-1);

    /**
     * 数据长度
     */
    private final Integer len;

    MsgFieldTypeEnums(int len) {
        this.len = len;
    }

    public Integer getLen() {
        return len;
    }

    @Override
    public String toString() {
        return "MsgFieldTypeEnums{" +
                "len=" + len +
                '}';
    }
}
