package com.nstskj.study.netty.tcp.netty.protocol.executor.msg;

import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base.AbstractInputNetTcpMessage;

/** 协议处理者工作执行器
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 22:01
 */
public interface ProtocolHandlerWorkExecutor {

    /**
     * 提交执行消息任务
     *
     * @param sessionId
     * @param abstractInputNetTcpMessage
     */
    boolean submit(String sessionId, AbstractInputNetTcpMessage abstractInputNetTcpMessage);

}
