package com.nstskj.study.netty.tcp.netty.protocol.session.enums;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 20:35
 * @Description 会话状态枚举
 */
public enum SessionStatusEnums {
    INIT,
    RUNING,
    CLOSE;
}
