package com.nstskj.study.netty.tcp.util;

import cn.hutool.core.util.HexUtil;
import com.google.protobuf.Message;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum;
import com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdErrRes;
import lombok.extern.slf4j.Slf4j;

/**
 * protobuf 工具类 ， 实现 json 及 字节数组 序列化
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/27 16:44
 */
@Slf4j
public class ProtoBufUtils {

    private ProtoBufUtils() {
    }

    /**
     * json 反序列化
     *
     * @param jsonStr
     * @param builder 消息的构造器
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T extends Message.Builder> Message toMassage(String jsonStr, T builder) throws Exception {
        JsonFormat.Parser parser = JsonFormat.parser();
        //对builder进行json反序列化
        parser.ignoringUnknownFields().merge(jsonStr, builder);
        return builder.build();
    }


    /**
     * 测试代码
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //创建对象都是采用建造者模式，Builder
        CommonCmdErrRes.Builder newBuilder = CommonCmdErrRes.newBuilder();
        CommonCmdErrRes commonCmdErrRes = newBuilder
                .setId("123")
                .setCmd("213")
                .setCmdStatus(CmdResStatusEnum.CMD_SUCCEED)
                .setCurrentTime(Timestamp.newBuilder()
                        .setSeconds(System.currentTimeMillis() / 1000)
                        .setNanos(0)
                )
                .setErrCode("123")
                .build();
        //调用tostring， 中文会被编码
        log.info("commonCmdErrRes  bean [{}]", commonCmdErrRes);
        //编码成字节数组
        log.info("commonCmdErrRes byte {}", HexUtil.encodeHexStr(commonCmdErrRes.toByteArray()));
        //反序列化
        final CommonCmdErrRes commonCmdErrRes1 = CommonCmdErrRes.parseFrom(commonCmdErrRes.toByteArray());
        //注意！直接调用javabean的tostring 方法，中文会被编码，直接获取属性值是正常的
        log.info("反序列 commonCmdErrRes1 bean [{}]", commonCmdErrRes1);
        // json 序列化
        //获取Printer对象用于生成JSON字符串 , 这里采用紧凑格式，不要空格缩进
        JsonFormat.Printer printer = JsonFormat.printer().omittingInsignificantWhitespace();
        String jsonStr = printer.print(commonCmdErrRes);
        log.info("转化为json字符串 {}", jsonStr);
        //获取parser对象用于解析JSON字符串
        JsonFormat.Parser parser = JsonFormat.parser();
        CommonCmdErrRes.Builder builder = CommonCmdErrRes.newBuilder();
        //对builder进行json反序列化
        parser.ignoringUnknownFields().merge(jsonStr, builder);
        CommonCmdErrRes commonCmdErrRes2 = builder.build();
        //注意！直接调用javabean的tostring 方法，中文会被编码,直接获取中文的属性是正常的， 返回为中文
        log.info("json反序列化为对象  bean [{}]", commonCmdErrRes2);
        //这里测试序列化和反序列化都是正常的
        Timestamp parse = Timestamps.parse("2023-10-27T09:04:50Z");
        log.info("parse--- {}",parse.getSeconds());
    }


}
