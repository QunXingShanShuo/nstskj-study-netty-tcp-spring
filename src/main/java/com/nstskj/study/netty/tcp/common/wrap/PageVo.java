package com.nstskj.study.netty.tcp.common.wrap;

import com.nstskj.study.netty.tcp.common.base.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 分页的vo包装类
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2020/5/4 12:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PageVo<T> extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 总记录数
     */
    @ApiModelProperty("总记录数")
    private long total;
    /**
     * 总页数
     */
    @ApiModelProperty("总页数")
    private long pageTotal;
    /**
     * 页码  从1 开始
     */
    @ApiModelProperty("页码 从1 开始")
    private long pageNum;
    /**
     * 页大小
     */
    @ApiModelProperty("页大小")
    private long pageSize;

    @ApiModelProperty("数据集合")
    private List<T> dataList;
}
