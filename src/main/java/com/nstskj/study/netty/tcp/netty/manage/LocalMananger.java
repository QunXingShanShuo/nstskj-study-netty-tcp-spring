package com.nstskj.study.netty.tcp.netty.manage;

import com.nstskj.study.netty.tcp.netty.manage.spring.LocalSpringBeanManager;
import lombok.Data;

/**
 * @author b053-mac
 * 本地服务管理
 */
@Data
public class LocalMananger {

    /**
     * 单例对象
     */
    public static final LocalMananger INSTANCE = new LocalMananger();

    private LocalMananger() {
    }

    /**
     * spring容器单例
     */
    private LocalSpringBeanManager localSpringBeanManager;


}
