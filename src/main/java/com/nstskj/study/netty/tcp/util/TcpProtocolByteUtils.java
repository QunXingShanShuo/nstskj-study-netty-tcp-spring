package com.nstskj.study.netty.tcp.util;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/3 19:07
 * @Description 字节数组操作工具类
 */
@Slf4j
public class TcpProtocolByteUtils {

    private TcpProtocolByteUtils() {
        throw new IllegalArgumentException("不能创建");
    }

    /**
     * 把字节数组转换为 short
     *
     * @param bytes
     * @return
     */
    public static short bytesToShort(byte[] bytes) {
        Assert.isTrue(bytes != null && bytes.length >= 2, "无效");
        return (short) ((short) bytes[0] * 256 + (short) bytes[1]);
    }


}

