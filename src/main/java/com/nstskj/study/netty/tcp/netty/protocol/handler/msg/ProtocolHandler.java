package com.nstskj.study.netty.tcp.netty.protocol.handler.msg;

import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base.AbstractInputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:03
 * @Description 协议处理者
 */
public interface ProtocolHandler {

    /**
     * 支持的协议
     *
     * @return
     */
    short supportCmd();

    /**
     * 处理cmd消息
     *
     * @param abstractInputNetTcpMessage
     * @return
     */
    AbstractOutputNetTcpMessage handlerCmdMsg(AbstractInputNetTcpMessage abstractInputNetTcpMessage);

}
