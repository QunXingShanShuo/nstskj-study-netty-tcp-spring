package com.nstskj.study.netty.tcp.netty.protocol.message.out.msg;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.HexUtil;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import lombok.Data;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 22:42
 * @Description 设备端点数据推送
 */
@Data
public class DeviceEndpointPushOutputNetTcpMessage extends AbstractOutputNetTcpMessage {
    /**
     * 端点数据
     */
    private List<EndpointData> endpointDataList;

    /**
     * 编码，自定义编码
     */
    @Override
    public void encodeCmdData() {
        if (CollUtil.isNotEmpty(endpointDataList)) {
            int len = endpointDataList.size() * 3;
            byte[] bytes = new byte[len];
            for (int i = 0; i < endpointDataList.size(); i++) {
                EndpointData endpointData = endpointDataList.get(i);
                short address = endpointData.getAddress();
                bytes[i * 3] = (byte) (address / 256);
                bytes[i * 3 + 1] = (byte) (address);
                bytes[i * 3 + 2] = HexUtil.decodeHex(endpointData.getHexStr())[0];
            }
            this.setCmdData(bytes);
        }
    }
}
