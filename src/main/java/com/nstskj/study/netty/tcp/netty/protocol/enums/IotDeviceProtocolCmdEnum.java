package com.nstskj.study.netty.tcp.netty.protocol.enums;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/3 20:02
 * @Description 协议命令枚举
 */
public enum IotDeviceProtocolCmdEnum {

    DEVICE_REGISTER(0X0005, 66, "设备注册"),
    DEVICE_ENDPOINT_PUSH(0X0006, 5, "设备端点信息上报");

    /**
     * 命令码
     */
    private final int cmd;

    /**
     * 命令数据域的最小值
     * 如果 -1 表示不限制
     */
    private final int cmdDataMinLen;

    /**
     * 命令描述
     */
    private final String desc;

    IotDeviceProtocolCmdEnum(int cmd, int minLen, String desc) {
        this.cmd = cmd;
        this.cmdDataMinLen = minLen;
        this.desc = desc;
    }

    public int getCmd() {
        return cmd;
    }

    public int getCmdDataMinLen() {
        return cmdDataMinLen;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "IotDeviceProtocolCmdEnum{" +
                "cmd=" + cmd +
                ", minLen=" + cmdDataMinLen +
                ", desc='" + desc + '\'' +
                '}';
    }
}
