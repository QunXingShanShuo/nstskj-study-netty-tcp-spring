package com.nstskj.study.netty.tcp.netty.protocol.definition.strategy;

import com.nstskj.study.netty.tcp.netty.protocol.definition.ProtocolMsgBeanDefinition;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:32
 * @Description 协议消息策略，通过命令码得到对应的消息描述
 */
public interface ProtocolMsgBeanDefinitionStrategy {

    /**
     * 得到指定的命令对应的bean定义
     *
     * @param cmd
     * @return
     */
    ProtocolMsgBeanDefinition getProtocolMsgBeanDefinition(short cmd);

}
