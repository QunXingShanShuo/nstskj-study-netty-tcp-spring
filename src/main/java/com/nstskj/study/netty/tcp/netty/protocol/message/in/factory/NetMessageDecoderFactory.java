package com.nstskj.study.netty.tcp.netty.protocol.message.in.factory;

import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base.AbstractInputNetTcpMessage;
import io.netty.buffer.ByteBuf;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 13:25
 * @Description 网络消息工厂
 */
public interface NetMessageDecoderFactory {

    /**
     * 解析报文，得到对应的消息bean
     *
     * @param byteBuf
     * @return
     */
    AbstractInputNetTcpMessage parse(ByteBuf byteBuf);

}
