package com.nstskj.study.netty.tcp.netty.protocol.handler.msg.strategy;

import com.nstskj.study.netty.tcp.netty.protocol.handler.msg.ProtocolHandler;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:38
 * @Description 协议处理策略
 */
public interface ProtocolHandlerStrategy {

    /**
     * 通过命令码得到对应的消息处理
     *
     * @param cmd
     * @return
     */
    ProtocolHandler getProtocolHandler(short cmd);


}
