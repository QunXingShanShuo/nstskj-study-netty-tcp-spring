package com.nstskj.study.netty.tcp.netty.protocol.endpoint.serialize;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 21:24
 * @Description 端口数据序列化和序列化
 */
public interface EndpointDataSerialize {

    /**
     * 序列化，把设备上传的数据转换为可识别的字符串
     * 并把结构写入到端点数据中
     *
     * @param abstractSession
     * @param abstractEndpoint
     * @param endpointData
     * @return
     */
    void serialize(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData);


    /**
     * 反序列化 ，把需要发送的数据转换为对应的命令报文
     *
     * @param abstractSession
     * @param abstractEndpoint
     * @param endpointData
     * @return
     */
    AbstractOutputNetTcpMessage deserialize(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData);

}
