package com.nstskj.study.netty.tcp.netty.channel.decoder;

import com.nstskj.study.netty.tcp.netty.manage.LocalMananger;
import com.nstskj.study.netty.tcp.netty.protocol.message.in.factory.NetMessageDecoderFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 13:23
 * @Description
 */
@Slf4j
public class NetTcpMessageDecoder extends MessageToMessageDecoder<ByteBuf> {

    private final NetMessageDecoderFactory netMessageDecoderFactory;

    public NetTcpMessageDecoder() {
        //得到解码工厂
        this.netMessageDecoderFactory = LocalMananger.INSTANCE.getLocalSpringBeanManager().getNetMessageDecoderFactory();
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        out.add(this.netMessageDecoderFactory.parse(msg));
    }
}
