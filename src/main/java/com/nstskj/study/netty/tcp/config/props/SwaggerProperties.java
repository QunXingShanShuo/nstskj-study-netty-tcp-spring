package com.nstskj.study.netty.tcp.config.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.util.CollectionUtils;

import java.util.*;


/**
 * swagger2 属性配置
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/14 11:19
 */
@Data
@ConfigurationProperties(prefix = SwaggerProperties.PREFIX)
public class SwaggerProperties {

    public static final String PREFIX = "swagger.config";

    /**
     * 是否开启swagger
     **/
    private boolean enabled = false;
    /**
     * 标题
     **/
    private String title = "";
    /**
     * 描述
     **/
    private String description = "";
    /**
     * 版本
     **/
    private String version = "";
    /**
     * 许可证
     **/
    private String license = "";
    /**
     * 许可证URL
     **/
    private String licenseUrl = "";
    /**
     * 服务条款URL
     **/
    private String termsOfServiceUrl = "";

    @NestedConfigurationProperty
    private Contact contact = new Contact();

    /**
     * swagger会解析的包路径
     **/
    private String basePackage = "";

    /**
     * swagger会解析的url规则
     **/
    private List<String> basePath = new ArrayList<>();
    /**
     * 在basePath基础上需要排除的url规则
     **/
    private List<String> excludePath = new ArrayList<>();

    /**
     * 分组文档
     **/
    @NestedConfigurationProperty
    private Map<String, DocketInfo> docket = new LinkedHashMap<>();

    /**
     * host信息
     **/
    private String host = "";

    /**
     * 全局参数配置
     **/
    @NestedConfigurationProperty
    private List<GlobalOperationParameter> globalOperationParameters = Collections.emptyList();

    /**
     * 全局默认的配置参数
     */
    private final List<GlobalOperationParameter> defaultGlobalOperationParameters = Arrays.asList(
            new GlobalOperationParameter() {
                {
                    setName("Authorization");
                    setDescription("认证的token bearer uuid ");
                    setParameterType("header");
                    setModelRef("string");
                    setRequired("false");
                }
            },
            new GlobalOperationParameter() {
                {
                    setName("u-tenant-header");
                    setDescription("租户id");
                    setParameterType("header");
                    setModelRef("string");
                    setRequired("false");
                }
            },
            new GlobalOperationParameter() {
                {
                    setName("x-sign-client-key");
                    setDescription("APP请求签名的key");
                    setParameterType("header");
                    setModelRef("string");
                    setRequired("false");
                }
            }
    );

    /**
     * 返回全局参数配置
     *
     * @return
     */
    public List<GlobalOperationParameter> globalOperationParameters() {
        final List<GlobalOperationParameter> globalOperations = new ArrayList<>(defaultGlobalOperationParameters);
        //属性配置文件配置
        if (!CollectionUtils.isEmpty(globalOperationParameters)) {
            globalOperations.addAll(globalOperationParameters);
        }
        return globalOperations;
    }


    @Data
    public static class GlobalOperationParameter {
        /**
         * 参数名
         **/
        private String name;

        /**
         * 描述信息
         **/
        private String description;

        /**
         * 指定参数类型
         **/
        private String modelRef;

        /**
         * 参数放在哪个地方:header,query,path,body.form
         **/
        private String parameterType;

        /**
         * 参数是否必须传
         **/
        private String required;
    }

    @Data
    public static class DocketInfo {
        /**
         * 标题
         **/
        private String title = "";
        /**
         * 描述
         **/
        private String description = "";
        /**
         * 版本
         **/
        private String version = "";
        /**
         * 许可证
         **/
        private String license = "";
        /**
         * 许可证URL
         **/
        private String licenseUrl = "";
        /**
         * 服务条款URL
         **/
        private String termsOfServiceUrl = "";

        private Contact contact = new Contact();

        /**
         * swagger会解析的包路径
         **/
        private String basePackage = "";

        /**
         * swagger会解析的url规则
         **/
        private List<String> basePath = new ArrayList<>();
        /**
         * 在basePath基础上需要排除的url规则
         **/
        private List<String> excludePath = new ArrayList<>();

        private List<GlobalOperationParameter> globalOperationParameters;
    }

    @Data
    public static class Contact {
        /**
         * 联系人
         **/
        private String name = "";
        /**
         * 联系人url
         **/
        private String url = "";
        /**
         * 联系人email
         **/
        private String email = "";
    }
}
