package com.nstskj.study.netty.tcp.netty.protocol.event;

import com.nstskj.study.netty.tcp.netty.protocol.event.base.AbstractNetDeviceEvent;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.Data;

/**
 * 设备注册事件
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:11
 * @Description
 */
@Data
public class RegisterNetDeviceEvent extends AbstractNetDeviceEvent {


    public RegisterNetDeviceEvent(Object source, AbstractSession abstractSession) {
        super(source, abstractSession);
    }
}
