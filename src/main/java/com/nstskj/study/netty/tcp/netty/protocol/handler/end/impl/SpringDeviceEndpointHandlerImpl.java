package com.nstskj.study.netty.tcp.netty.protocol.handler.end.impl;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.handler.end.AbstractDeviceEndpointHandler;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 20:58
 * @Description
 */
@Slf4j
@Component
public class SpringDeviceEndpointHandlerImpl extends AbstractDeviceEndpointHandler {

    @Override
    public void endpointHandler(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData) {

    }
}
