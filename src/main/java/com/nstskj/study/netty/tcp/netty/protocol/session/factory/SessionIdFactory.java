package com.nstskj.study.netty.tcp.netty.protocol.session.factory;

import io.netty.channel.Channel;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:01
 * @Description 会话id工厂
 */
public interface SessionIdFactory {

    /**
     * 创建session id
     *
     * @param channel
     * @return
     */
    String createSessionId(Channel channel);


}
