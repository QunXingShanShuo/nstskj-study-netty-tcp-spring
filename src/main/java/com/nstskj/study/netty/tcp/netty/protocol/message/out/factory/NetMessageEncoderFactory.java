package com.nstskj.study.netty.tcp.netty.protocol.message.out.factory;

import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import io.netty.buffer.ByteBuf;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 13:25
 * @Description 网络消息编码器工厂
 */
public interface NetMessageEncoderFactory {

    /**
     * 把响应报文编码为字节流
     *
     * @param netTcpMessage
     * @return
     */
    ByteBuf createByteBuf(AbstractOutputNetTcpMessage netTcpMessage);

}
