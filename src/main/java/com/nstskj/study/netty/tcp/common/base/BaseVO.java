package com.nstskj.study.netty.tcp.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用的响应数据格式泛型
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/14 10:46
 */
@Data
public class BaseVO implements Serializable {
    private static final long serialVersionUID = -1L;
}
