package com.nstskj.study.netty.tcp.common.vo;

import com.nstskj.study.netty.tcp.common.base.BaseVO;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/23 11:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GetAddressDataBySessionIdVO extends BaseVO {
    private static final long serialVersionUID = -1L;

    private List<EndpointData> endpointDataList;
}
