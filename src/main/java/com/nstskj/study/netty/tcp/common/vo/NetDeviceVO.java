package com.nstskj.study.netty.tcp.common.vo;

import com.nstskj.study.netty.tcp.common.base.BaseVO;
import com.nstskj.study.netty.tcp.netty.protocol.session.NetDeviceSession;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 网络设备
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/22 21:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NetDeviceVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 主机
     */
    private String hosts;

    /**
     * 设备会话
     */
    private NetDeviceSession netDeviceSession;


}
