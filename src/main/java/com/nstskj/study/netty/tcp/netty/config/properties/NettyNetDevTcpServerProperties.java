package com.nstskj.study.netty.tcp.netty.config.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 10:06
 * @Description netty服务器参数配置
 */
@Slf4j
@Data
@ConfigurationProperties(NettyNetDevTcpServerProperties.PREFIX)
public class NettyNetDevTcpServerProperties {
    public static final String PREFIX = "netty.tcp.server";

    /**
     * netty服务器配置
     */
    @NestedConfigurationProperty
    private NettyBootServerProperties nettyBootServerProperties = new NettyBootServerProperties();

    /**
     * 协议处理的线程池配置
     */
    @NestedConfigurationProperty
    private ThreadExecutionProperties protocolHandlerWorkExecutor = new ThreadExecutionProperties();

    /**
     * 设备端点数据工作者线程池配置
     */
    @NestedConfigurationProperty
    private ThreadExecutionProperties deviceEndpointDataWorkExecutor = new ThreadExecutionProperties();
}
