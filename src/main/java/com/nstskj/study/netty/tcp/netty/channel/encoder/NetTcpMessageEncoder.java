package com.nstskj.study.netty.tcp.netty.channel.encoder;

import com.nstskj.study.netty.tcp.netty.manage.LocalMananger;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 22:56
 * @Description 编码器
 */
@Slf4j
public class NetTcpMessageEncoder extends MessageToMessageEncoder<AbstractOutputNetTcpMessage> {

    @Override
    protected void encode(ChannelHandlerContext ctx, AbstractOutputNetTcpMessage msg, List<Object> out) throws Exception {
        ByteBuf byteBuf = LocalMananger.INSTANCE.getLocalSpringBeanManager().getNetMessageEncoderFactory().createByteBuf(msg);
        if (byteBuf != null) {
            out.add(byteBuf);
        }
    }
}
