// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: common.proto

package com.nstskj.study.netty.tcp.message.protobuf.common;

/**
 * <pre>
 * 通用的命令响应
 * </pre>
 *
 * Protobuf type {@code net.common.CommonCmdRes}
 */
public final class CommonCmdRes extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:net.common.CommonCmdRes)
    CommonCmdResOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CommonCmdRes.newBuilder() to construct.
  private CommonCmdRes(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CommonCmdRes() {
    id_ = "";
    cmd_ = "";
    cmdStatus_ = 0;
    msg_ = "";
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new CommonCmdRes();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.nstskj.study.netty.tcp.message.protobuf.common.CommonMessageProBuf.internal_static_net_common_CommonCmdRes_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.nstskj.study.netty.tcp.message.protobuf.common.CommonMessageProBuf.internal_static_net_common_CommonCmdRes_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.class, com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.Builder.class);
  }

  private int bitField0_;
  public static final int ID_FIELD_NUMBER = 1;
  @SuppressWarnings("serial")
  private volatile java.lang.Object id_ = "";
  /**
   * <pre>
   * 请求命令的id，用于追溯
   * </pre>
   *
   * <code>string id = 1;</code>
   * @return The id.
   */
  @java.lang.Override
  public java.lang.String getId() {
    java.lang.Object ref = id_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      id_ = s;
      return s;
    }
  }
  /**
   * <pre>
   * 请求命令的id，用于追溯
   * </pre>
   *
   * <code>string id = 1;</code>
   * @return The bytes for id.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getIdBytes() {
    java.lang.Object ref = id_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      id_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int CMD_FIELD_NUMBER = 2;
  @SuppressWarnings("serial")
  private volatile java.lang.Object cmd_ = "";
  /**
   * <pre>
   *命令码
   * </pre>
   *
   * <code>string cmd = 2;</code>
   * @return The cmd.
   */
  @java.lang.Override
  public java.lang.String getCmd() {
    java.lang.Object ref = cmd_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      cmd_ = s;
      return s;
    }
  }
  /**
   * <pre>
   *命令码
   * </pre>
   *
   * <code>string cmd = 2;</code>
   * @return The bytes for cmd.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getCmdBytes() {
    java.lang.Object ref = cmd_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      cmd_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int CURRENTTIME_FIELD_NUMBER = 3;
  private com.google.protobuf.Timestamp currentTime_;
  /**
   * <pre>
   *时间戳
   * </pre>
   *
   * <code>.google.protobuf.Timestamp currentTime = 3;</code>
   * @return Whether the currentTime field is set.
   */
  @java.lang.Override
  public boolean hasCurrentTime() {
    return ((bitField0_ & 0x00000001) != 0);
  }
  /**
   * <pre>
   *时间戳
   * </pre>
   *
   * <code>.google.protobuf.Timestamp currentTime = 3;</code>
   * @return The currentTime.
   */
  @java.lang.Override
  public com.google.protobuf.Timestamp getCurrentTime() {
    return currentTime_ == null ? com.google.protobuf.Timestamp.getDefaultInstance() : currentTime_;
  }
  /**
   * <pre>
   *时间戳
   * </pre>
   *
   * <code>.google.protobuf.Timestamp currentTime = 3;</code>
   */
  @java.lang.Override
  public com.google.protobuf.TimestampOrBuilder getCurrentTimeOrBuilder() {
    return currentTime_ == null ? com.google.protobuf.Timestamp.getDefaultInstance() : currentTime_;
  }

  public static final int CMDSTATUS_FIELD_NUMBER = 4;
  private int cmdStatus_ = 0;
  /**
   * <pre>
   *命令的执行状态
   * </pre>
   *
   * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
   * @return The enum numeric value on the wire for cmdStatus.
   */
  @java.lang.Override public int getCmdStatusValue() {
    return cmdStatus_;
  }
  /**
   * <pre>
   *命令的执行状态
   * </pre>
   *
   * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
   * @return The cmdStatus.
   */
  @java.lang.Override public com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum getCmdStatus() {
    com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum result = com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.forNumber(cmdStatus_);
    return result == null ? com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.UNRECOGNIZED : result;
  }

  public static final int MSG_FIELD_NUMBER = 5;
  @SuppressWarnings("serial")
  private volatile java.lang.Object msg_ = "";
  /**
   * <pre>
   *错误消息
   * </pre>
   *
   * <code>optional string msg = 5;</code>
   * @return Whether the msg field is set.
   */
  @java.lang.Override
  public boolean hasMsg() {
    return ((bitField0_ & 0x00000002) != 0);
  }
  /**
   * <pre>
   *错误消息
   * </pre>
   *
   * <code>optional string msg = 5;</code>
   * @return The msg.
   */
  @java.lang.Override
  public java.lang.String getMsg() {
    java.lang.Object ref = msg_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      msg_ = s;
      return s;
    }
  }
  /**
   * <pre>
   *错误消息
   * </pre>
   *
   * <code>optional string msg = 5;</code>
   * @return The bytes for msg.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getMsgBytes() {
    java.lang.Object ref = msg_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      msg_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(id_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 1, id_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(cmd_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, cmd_);
    }
    if (((bitField0_ & 0x00000001) != 0)) {
      output.writeMessage(3, getCurrentTime());
    }
    if (cmdStatus_ != com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.CMD_UN_KNOW.getNumber()) {
      output.writeEnum(4, cmdStatus_);
    }
    if (((bitField0_ & 0x00000002) != 0)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 5, msg_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(id_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, id_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(cmd_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, cmd_);
    }
    if (((bitField0_ & 0x00000001) != 0)) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getCurrentTime());
    }
    if (cmdStatus_ != com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.CMD_UN_KNOW.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(4, cmdStatus_);
    }
    if (((bitField0_ & 0x00000002) != 0)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(5, msg_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes)) {
      return super.equals(obj);
    }
    com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes other = (com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes) obj;

    if (!getId()
        .equals(other.getId())) return false;
    if (!getCmd()
        .equals(other.getCmd())) return false;
    if (hasCurrentTime() != other.hasCurrentTime()) return false;
    if (hasCurrentTime()) {
      if (!getCurrentTime()
          .equals(other.getCurrentTime())) return false;
    }
    if (cmdStatus_ != other.cmdStatus_) return false;
    if (hasMsg() != other.hasMsg()) return false;
    if (hasMsg()) {
      if (!getMsg()
          .equals(other.getMsg())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + ID_FIELD_NUMBER;
    hash = (53 * hash) + getId().hashCode();
    hash = (37 * hash) + CMD_FIELD_NUMBER;
    hash = (53 * hash) + getCmd().hashCode();
    if (hasCurrentTime()) {
      hash = (37 * hash) + CURRENTTIME_FIELD_NUMBER;
      hash = (53 * hash) + getCurrentTime().hashCode();
    }
    hash = (37 * hash) + CMDSTATUS_FIELD_NUMBER;
    hash = (53 * hash) + cmdStatus_;
    if (hasMsg()) {
      hash = (37 * hash) + MSG_FIELD_NUMBER;
      hash = (53 * hash) + getMsg().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }

  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * 通用的命令响应
   * </pre>
   *
   * Protobuf type {@code net.common.CommonCmdRes}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:net.common.CommonCmdRes)
      com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdResOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.nstskj.study.netty.tcp.message.protobuf.common.CommonMessageProBuf.internal_static_net_common_CommonCmdRes_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.nstskj.study.netty.tcp.message.protobuf.common.CommonMessageProBuf.internal_static_net_common_CommonCmdRes_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.class, com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.Builder.class);
    }

    // Construct using com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getCurrentTimeFieldBuilder();
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      id_ = "";
      cmd_ = "";
      currentTime_ = null;
      if (currentTimeBuilder_ != null) {
        currentTimeBuilder_.dispose();
        currentTimeBuilder_ = null;
      }
      cmdStatus_ = 0;
      msg_ = "";
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.nstskj.study.netty.tcp.message.protobuf.common.CommonMessageProBuf.internal_static_net_common_CommonCmdRes_descriptor;
    }

    @java.lang.Override
    public com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes getDefaultInstanceForType() {
      return com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.getDefaultInstance();
    }

    @java.lang.Override
    public com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes build() {
      com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes buildPartial() {
      com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes result = new com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.id_ = id_;
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.cmd_ = cmd_;
      }
      int to_bitField0_ = 0;
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.currentTime_ = currentTimeBuilder_ == null
            ? currentTime_
            : currentTimeBuilder_.build();
        to_bitField0_ |= 0x00000001;
      }
      if (((from_bitField0_ & 0x00000008) != 0)) {
        result.cmdStatus_ = cmdStatus_;
      }
      if (((from_bitField0_ & 0x00000010) != 0)) {
        result.msg_ = msg_;
        to_bitField0_ |= 0x00000002;
      }
      result.bitField0_ |= to_bitField0_;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes) {
        return mergeFrom((com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes other) {
      if (other == com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes.getDefaultInstance()) return this;
      if (!other.getId().isEmpty()) {
        id_ = other.id_;
        bitField0_ |= 0x00000001;
        onChanged();
      }
      if (!other.getCmd().isEmpty()) {
        cmd_ = other.cmd_;
        bitField0_ |= 0x00000002;
        onChanged();
      }
      if (other.hasCurrentTime()) {
        mergeCurrentTime(other.getCurrentTime());
      }
      if (other.cmdStatus_ != 0) {
        setCmdStatusValue(other.getCmdStatusValue());
      }
      if (other.hasMsg()) {
        msg_ = other.msg_;
        bitField0_ |= 0x00000010;
        onChanged();
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              id_ = input.readStringRequireUtf8();
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              cmd_ = input.readStringRequireUtf8();
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getCurrentTimeFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            case 32: {
              cmdStatus_ = input.readEnum();
              bitField0_ |= 0x00000008;
              break;
            } // case 32
            case 42: {
              msg_ = input.readStringRequireUtf8();
              bitField0_ |= 0x00000010;
              break;
            } // case 42
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private java.lang.Object id_ = "";
    /**
     * <pre>
     * 请求命令的id，用于追溯
     * </pre>
     *
     * <code>string id = 1;</code>
     * @return The id.
     */
    public java.lang.String getId() {
      java.lang.Object ref = id_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        id_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <pre>
     * 请求命令的id，用于追溯
     * </pre>
     *
     * <code>string id = 1;</code>
     * @return The bytes for id.
     */
    public com.google.protobuf.ByteString
        getIdBytes() {
      java.lang.Object ref = id_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        id_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <pre>
     * 请求命令的id，用于追溯
     * </pre>
     *
     * <code>string id = 1;</code>
     * @param value The id to set.
     * @return This builder for chaining.
     */
    public Builder setId(
        java.lang.String value) {
      if (value == null) { throw new NullPointerException(); }
      id_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * 请求命令的id，用于追溯
     * </pre>
     *
     * <code>string id = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearId() {
      id_ = getDefaultInstance().getId();
      bitField0_ = (bitField0_ & ~0x00000001);
      onChanged();
      return this;
    }
    /**
     * <pre>
     * 请求命令的id，用于追溯
     * </pre>
     *
     * <code>string id = 1;</code>
     * @param value The bytes for id to set.
     * @return This builder for chaining.
     */
    public Builder setIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      checkByteStringIsUtf8(value);
      id_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }

    private java.lang.Object cmd_ = "";
    /**
     * <pre>
     *命令码
     * </pre>
     *
     * <code>string cmd = 2;</code>
     * @return The cmd.
     */
    public java.lang.String getCmd() {
      java.lang.Object ref = cmd_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        cmd_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <pre>
     *命令码
     * </pre>
     *
     * <code>string cmd = 2;</code>
     * @return The bytes for cmd.
     */
    public com.google.protobuf.ByteString
        getCmdBytes() {
      java.lang.Object ref = cmd_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        cmd_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <pre>
     *命令码
     * </pre>
     *
     * <code>string cmd = 2;</code>
     * @param value The cmd to set.
     * @return This builder for chaining.
     */
    public Builder setCmd(
        java.lang.String value) {
      if (value == null) { throw new NullPointerException(); }
      cmd_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *命令码
     * </pre>
     *
     * <code>string cmd = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearCmd() {
      cmd_ = getDefaultInstance().getCmd();
      bitField0_ = (bitField0_ & ~0x00000002);
      onChanged();
      return this;
    }
    /**
     * <pre>
     *命令码
     * </pre>
     *
     * <code>string cmd = 2;</code>
     * @param value The bytes for cmd to set.
     * @return This builder for chaining.
     */
    public Builder setCmdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      checkByteStringIsUtf8(value);
      cmd_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }

    private com.google.protobuf.Timestamp currentTime_;
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder> currentTimeBuilder_;
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     * @return Whether the currentTime field is set.
     */
    public boolean hasCurrentTime() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     * @return The currentTime.
     */
    public com.google.protobuf.Timestamp getCurrentTime() {
      if (currentTimeBuilder_ == null) {
        return currentTime_ == null ? com.google.protobuf.Timestamp.getDefaultInstance() : currentTime_;
      } else {
        return currentTimeBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public Builder setCurrentTime(com.google.protobuf.Timestamp value) {
      if (currentTimeBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        currentTime_ = value;
      } else {
        currentTimeBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public Builder setCurrentTime(
        com.google.protobuf.Timestamp.Builder builderForValue) {
      if (currentTimeBuilder_ == null) {
        currentTime_ = builderForValue.build();
      } else {
        currentTimeBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public Builder mergeCurrentTime(com.google.protobuf.Timestamp value) {
      if (currentTimeBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          currentTime_ != null &&
          currentTime_ != com.google.protobuf.Timestamp.getDefaultInstance()) {
          getCurrentTimeBuilder().mergeFrom(value);
        } else {
          currentTime_ = value;
        }
      } else {
        currentTimeBuilder_.mergeFrom(value);
      }
      if (currentTime_ != null) {
        bitField0_ |= 0x00000004;
        onChanged();
      }
      return this;
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public Builder clearCurrentTime() {
      bitField0_ = (bitField0_ & ~0x00000004);
      currentTime_ = null;
      if (currentTimeBuilder_ != null) {
        currentTimeBuilder_.dispose();
        currentTimeBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public com.google.protobuf.Timestamp.Builder getCurrentTimeBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getCurrentTimeFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    public com.google.protobuf.TimestampOrBuilder getCurrentTimeOrBuilder() {
      if (currentTimeBuilder_ != null) {
        return currentTimeBuilder_.getMessageOrBuilder();
      } else {
        return currentTime_ == null ?
            com.google.protobuf.Timestamp.getDefaultInstance() : currentTime_;
      }
    }
    /**
     * <pre>
     *时间戳
     * </pre>
     *
     * <code>.google.protobuf.Timestamp currentTime = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder> 
        getCurrentTimeFieldBuilder() {
      if (currentTimeBuilder_ == null) {
        currentTimeBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder>(
                getCurrentTime(),
                getParentForChildren(),
                isClean());
        currentTime_ = null;
      }
      return currentTimeBuilder_;
    }

    private int cmdStatus_ = 0;
    /**
     * <pre>
     *命令的执行状态
     * </pre>
     *
     * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
     * @return The enum numeric value on the wire for cmdStatus.
     */
    @java.lang.Override public int getCmdStatusValue() {
      return cmdStatus_;
    }
    /**
     * <pre>
     *命令的执行状态
     * </pre>
     *
     * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
     * @param value The enum numeric value on the wire for cmdStatus to set.
     * @return This builder for chaining.
     */
    public Builder setCmdStatusValue(int value) {
      cmdStatus_ = value;
      bitField0_ |= 0x00000008;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *命令的执行状态
     * </pre>
     *
     * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
     * @return The cmdStatus.
     */
    @java.lang.Override
    public com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum getCmdStatus() {
      com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum result = com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.forNumber(cmdStatus_);
      return result == null ? com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum.UNRECOGNIZED : result;
    }
    /**
     * <pre>
     *命令的执行状态
     * </pre>
     *
     * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
     * @param value The cmdStatus to set.
     * @return This builder for chaining.
     */
    public Builder setCmdStatus(com.nstskj.study.netty.tcp.message.protobuf.common.CmdResStatusEnum value) {
      if (value == null) {
        throw new NullPointerException();
      }
      bitField0_ |= 0x00000008;
      cmdStatus_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <pre>
     *命令的执行状态
     * </pre>
     *
     * <code>.net.common.CmdResStatusEnum cmdStatus = 4;</code>
     * @return This builder for chaining.
     */
    public Builder clearCmdStatus() {
      bitField0_ = (bitField0_ & ~0x00000008);
      cmdStatus_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object msg_ = "";
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @return Whether the msg field is set.
     */
    public boolean hasMsg() {
      return ((bitField0_ & 0x00000010) != 0);
    }
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @return The msg.
     */
    public java.lang.String getMsg() {
      java.lang.Object ref = msg_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        msg_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @return The bytes for msg.
     */
    public com.google.protobuf.ByteString
        getMsgBytes() {
      java.lang.Object ref = msg_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        msg_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @param value The msg to set.
     * @return This builder for chaining.
     */
    public Builder setMsg(
        java.lang.String value) {
      if (value == null) { throw new NullPointerException(); }
      msg_ = value;
      bitField0_ |= 0x00000010;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @return This builder for chaining.
     */
    public Builder clearMsg() {
      msg_ = getDefaultInstance().getMsg();
      bitField0_ = (bitField0_ & ~0x00000010);
      onChanged();
      return this;
    }
    /**
     * <pre>
     *错误消息
     * </pre>
     *
     * <code>optional string msg = 5;</code>
     * @param value The bytes for msg to set.
     * @return This builder for chaining.
     */
    public Builder setMsgBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      checkByteStringIsUtf8(value);
      msg_ = value;
      bitField0_ |= 0x00000010;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:net.common.CommonCmdRes)
  }

  // @@protoc_insertion_point(class_scope:net.common.CommonCmdRes)
  private static final com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes();
  }

  public static com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CommonCmdRes>
      PARSER = new com.google.protobuf.AbstractParser<CommonCmdRes>() {
    @java.lang.Override
    public CommonCmdRes parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<CommonCmdRes> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CommonCmdRes> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.nstskj.study.netty.tcp.message.protobuf.common.CommonCmdRes getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

