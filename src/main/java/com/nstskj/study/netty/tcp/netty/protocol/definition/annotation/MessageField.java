package com.nstskj.study.netty.tcp.netty.protocol.definition.annotation;

import com.nstskj.study.netty.tcp.netty.protocol.definition.enums.MsgFieldTypeEnums;

import java.lang.annotation.*;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:18
 * @Description 消息字段定义
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageField {

    /**
     * 字段类型
     *
     * @return
     */
    MsgFieldTypeEnums fieldType();

    /**
     * 起始索引
     *
     * @return
     */
    int startIndex();

    /**
     * 字段长度
     *
     * @return
     */
    int length() default -1;

    /**
     * 是否为小端模式
     * 只有为整形时才有效
     *
     * @return
     */
    boolean isLittleEndian() default true;

}

