package com.nstskj.study.netty.tcp.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 响应结果封装
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2023/10/13 20:18
 */
@Data
public class Result<T extends BaseVO> implements Serializable {

    private static final long serialVersionUID = -1L;

    public static final int SUCCEED_CODE = 200;

    public static final int ERR_CODE = -1;

    @ApiModelProperty("错误码 200 成功")
    private final int code;

    @ApiModelProperty("错误描述")
    private final String msg;

    @ApiModelProperty("响应的载荷")
    private final T data;

    private Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @ApiModelProperty("请求是否执行成功与code==200等价")
    public boolean isSucceed() {
        return this.code == SUCCEED_CODE;
    }

    public static <T extends BaseVO> Result<T> ok(T data) {
        return new Result<T>(SUCCEED_CODE, "ok", data);
    }


    public static <T extends BaseVO> Result<T> err(int code, String msg) {
        return new Result<T>(code == SUCCEED_CODE ? ERR_CODE : code, msg, null);
    }


    public static <T extends BaseVO> Result<T> err(String msg) {
        return new Result<T>(ERR_CODE, msg, null);
    }

}
