package com.nstskj.study.netty.tcp.netty.protocol.session.factory.impl;

import com.nstskj.study.netty.tcp.netty.protocol.session.factory.SessionIdFactory;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:02
 * @Description
 */
@Slf4j
@Component
public class SpringSessionIdFactory implements SessionIdFactory {

    private final AtomicInteger atomicInteger = new AtomicInteger();

    /**
     * 创建会话id
     *
     * @param channel
     * @return
     */
    @Override
    public String createSessionId(Channel channel) {
        return "" + atomicInteger.incrementAndGet();
    }
}
