package com.nstskj.study.netty.tcp.service;

import com.nstskj.study.netty.tcp.common.vo.NetDeviceVO;
import com.nstskj.study.netty.tcp.common.wrap.PageVo;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/22 21:23
 * @Description
 */
public interface NetDeviceService {
    /**
     * 得到设备列表
     *
     * @return
     */
    PageVo<NetDeviceVO> getNetDeviceList();

    /**
     * 关闭指定的设备
     *
     * @param sessionId
     * @return
     */
    boolean closeNetDeviceBySessionId(String sessionId);

    /**
     * 向指定的设备端点地址发送数据
     *
     * @param sessionId
     * @param address
     * @param data
     * @return
     */
    boolean sendAddressDataBySessionId(String sessionId, Short address, Short data);

    /**
     * 读取指定会话的端点数据
     *
     * @param sessionId
     * @param address
     * @return
     */
    List<EndpointData> getAddressDataBySessionId(String sessionId, Short address);
}
