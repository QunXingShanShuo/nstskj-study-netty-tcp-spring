package com.nstskj.study.netty.tcp.netty.protocol.annotation;

import com.nstskj.study.netty.tcp.netty.protocol.enums.IotDeviceProtocolCmdEnum;

import java.lang.annotation.*;

/**
 * 协议命令类型
 *
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:02
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageCommandAnnotation {
    /**
     * 命令码
     *
     * @return
     */
    IotDeviceProtocolCmdEnum value();

}
