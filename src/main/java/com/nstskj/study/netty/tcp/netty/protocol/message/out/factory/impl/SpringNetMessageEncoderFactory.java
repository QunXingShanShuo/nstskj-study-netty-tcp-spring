package com.nstskj.study.netty.tcp.netty.protocol.message.out.factory.impl;

import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.factory.NetMessageEncoderFactory;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.EncoderException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 13:27
 * @Description
 */
@Slf4j
@Component
public class SpringNetMessageEncoderFactory implements NetMessageEncoderFactory {

    /**
     * 把响应报文编码为字节流
     *
     * @param netTcpMessage
     * @return
     */
    @Override
    public ByteBuf createByteBuf(AbstractOutputNetTcpMessage netTcpMessage) {
        try {
            //对命令数据进行编码
            netTcpMessage.encodeCmdData();
        } catch (Exception e) {
            log.info("执行数据域编码错误");
            throw new EncoderException("编码错误");
        }
        ByteBuf buffer = Unpooled.buffer(255);
        buffer.writeShort(netTcpMessage.getHead());
        //这里先写一个初始值，后面再改
        buffer.writeInt(0);
        buffer.writeBytes(netTcpMessage.getVersion().getBytes());
        buffer.writeShort(netTcpMessage.getCmd());
        buffer.writeInt(netTcpMessage.getSerial());
        buffer.writeBytes(netTcpMessage.getCmdData());
        int length = netTcpMessage.getCmdData().length + 8;
        buffer.setInt(2, length);
        return buffer;
    }
}
