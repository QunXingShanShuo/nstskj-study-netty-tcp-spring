package com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base;

import lombok.Data;
import com.nstskj.study.netty.tcp.netty.protocol.common.base.AbstractBaseProtocol;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 20:49
 * @Description 抽象的输入消息
 */
@Data
public abstract class AbstractInputNetTcpMessage extends AbstractBaseProtocol {

    /**
     * 命令数据自定义处理
     */
    public abstract void decoderCmdData();

}
