package com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base;


import com.nstskj.study.netty.tcp.netty.protocol.common.base.AbstractBaseProtocol;
import lombok.Data;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:04
 * @Description
 */
@Data
public abstract class AbstractOutputNetTcpMessage extends AbstractBaseProtocol {

    /**
     * 编码，自定义编码
     */
    public abstract void encodeCmdData();

}
