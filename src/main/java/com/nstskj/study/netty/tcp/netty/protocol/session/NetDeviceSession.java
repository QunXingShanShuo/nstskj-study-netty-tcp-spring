package com.nstskj.study.netty.tcp.netty.protocol.session;

import com.nstskj.study.netty.tcp.netty.manage.LocalMananger;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.DeviceEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.event.CloseNetDeviceEvent;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import com.nstskj.study.netty.tcp.netty.protocol.session.enums.SessionStatusEnums;
import io.netty.channel.Channel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 20:47
 * @Description 网络设备消息
 */
@Data
public class NetDeviceSession extends AbstractSession {

    /**
     * 构造器
     *
     * @param sessionId
     * @param channel
     */
    public NetDeviceSession(String sessionId, Channel channel) {
        super(sessionId, channel);
        this.setStatus(SessionStatusEnums.INIT);
    }

    /**
     * 关闭方法
     */
    @Override
    public void close() {
        this.setStatus(SessionStatusEnums.CLOSE);
        CloseNetDeviceEvent closeNetDeviceEvent = new CloseNetDeviceEvent(this, this);
        //发送关闭事件
        LocalMananger.INSTANCE.getLocalSpringBeanManager().getApplicationContext().publishEvent(closeNetDeviceEvent);
        Channel channel = this.getChannel();
        if (channel != null && channel.isActive()) {
            channel.close();
        }
    }
}
