package com.nstskj.study.netty.tcp.util;

import com.nstskj.study.netty.tcp.netty.constants.NettyProtocolConstants;
import com.nstskj.study.netty.tcp.netty.manage.LocalMananger;
import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.DeviceRegisterCmdMsg;
import com.nstskj.study.netty.tcp.netty.protocol.message.in.msg.base.AbstractInputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import com.nstskj.study.netty.tcp.netty.protocol.session.NetDeviceSession;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.extern.slf4j.Slf4j;

/** 会话工具类
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 9:00
 */
@Slf4j
public class TcpSessionUtils {

    private TcpSessionUtils() {
        throw new IllegalArgumentException("不允许");
    }

    /**
     * 从tcp消息中得到会话id
     *
     * @param abstractInputNetTcpMessage
     * @return
     */
    public static String getSessionIdByTcpMessage(AbstractInputNetTcpMessage abstractInputNetTcpMessage) {
        return abstractInputNetTcpMessage.attr(NettyProtocolConstants.CHANNEL_SESSION_ID).get();
    }

    /**
     * 从tcp消息中得到会话对象
     *
     * @param abstractInputNetTcpMessage
     * @return
     */
    public static AbstractSession getSessionByTcpMessage(AbstractInputNetTcpMessage abstractInputNetTcpMessage) {
        return LocalMananger.INSTANCE.getLocalSpringBeanManager().getNetDeviceSessionManage().getSession(getSessionIdByTcpMessage(abstractInputNetTcpMessage));
    }
}