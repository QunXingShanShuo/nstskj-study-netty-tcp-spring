package com.nstskj.study.netty.tcp.netty.config;

import com.nstskj.study.netty.tcp.netty.config.properties.NettyNetDevTcpServerProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 10:07
 * @Description
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
public class NettyTcpBootConfiguration {

    @Bean
    public NettyNetDevTcpServerProperties nettyNetDevTcpServerProperties() {
        return new NettyNetDevTcpServerProperties();
    }

}
