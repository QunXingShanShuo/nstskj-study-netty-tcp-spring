package com.nstskj.study.netty.tcp.netty.protocol.definition;

import com.nstskj.study.netty.tcp.netty.protocol.definition.enums.MsgFieldTypeEnums;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 21:12
 * @Description 协议消息字段定义
 */
@Data
public class ProtocolMsgFieldDefinition implements Serializable, Comparable<ProtocolMsgFieldDefinition> {
    private static final long serialVersionUID = 1L;

    /**
     * 字段的class名称
     */
    private String className;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段类型
     */
    private MsgFieldTypeEnums fieldTypeEnums;

    /**
     * 起始索引
     */
    private int fieldStartIndex;

    /**
     * 字段长度
     */
    private int fieldLen;

    /**
     * 是否为小端模式
     */
    private boolean isLittleEndian;

    /**
     * 排序
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(ProtocolMsgFieldDefinition o) {
        return Integer.compare(this.getFieldStartIndex(), o.getFieldStartIndex());
    }
}
