package com.nstskj.study.netty.tcp.netty.protocol.message.out.msg;

import com.nstskj.study.netty.tcp.netty.protocol.message.out.msg.base.AbstractOutputNetTcpMessage;
import lombok.Data;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 22:42
 * @Description 设备注册响应事件
 */
@Data
public class DeviceRegisterOutputNetTcpMessage extends AbstractOutputNetTcpMessage {

    private byte status;

    @Override
    public void encodeCmdData() {
        this.setCmdData(new byte[]{status});
    }
}
