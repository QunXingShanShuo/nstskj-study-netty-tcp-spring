package com.nstskj.study.netty.tcp.netty.protocol.endpoint.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/3 17:02
 * @Description 端点数据
 */
@Data
public class EndpointData implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 接收到的时间搓
     * {@link System#currentTimeMillis() }
     */
    private long timestamp;

    /**
     * 接收到的时间搓
     * {@link cn.hutool.core.date.DatePattern#NORM_DATETIME_FORMAT }
     */
    private String timestampStr;
    /**
     * 端点的地址
     */
    private short address;

    /**
     * 原始数据对应的hex字符串
     */
    private String hexStr;

    /**
     * 解析完成后的数据
     */
    private String resultData;

}
