package com.nstskj.study.netty.tcp.netty.protocol.event.base;

import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:08
 * @Description 抽象的网络设备消息
 */
@Data
public abstract class AbstractNetDeviceEvent extends ApplicationEvent {

    private static final long serialVersionUID = 1L;

    /**
     * 会话对象
     */
    private final AbstractSession abstractSession;

    /**
     * 创建事件
     *
     * @param source
     * @param abstractSession
     */
    public AbstractNetDeviceEvent(Object source, AbstractSession abstractSession) {
        super(source);
        this.abstractSession = abstractSession;
    }
}
