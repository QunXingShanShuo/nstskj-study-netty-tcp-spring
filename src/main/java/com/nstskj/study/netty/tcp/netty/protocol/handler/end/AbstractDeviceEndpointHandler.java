package com.nstskj.study.netty.tcp.netty.protocol.handler.end;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 19:41
 * @Description 抽象的端点数据处理
 */
@Slf4j
public abstract class AbstractDeviceEndpointHandler implements DeviceEndpointHandler {

    /**
     * 这里把端点分为输入和输出两类
     *
     * @param abstractSession  会话
     * @param abstractEndpoint 端点对象
     * @param endpointData     端点数据
     */
    @Override
    public void endpointHandler(AbstractSession abstractSession, AbstractEndpoint abstractEndpoint, EndpointData endpointData) {
        if (abstractEndpoint.isOut()) {
            //如果为输出端点 ,则需要把端点数据转换为对应的响应对象报文

        } else {
            //如果为输入端点，则需要把接收到的报文反序列化
        }
    }
}
