package com.nstskj.study.netty.tcp.netty.protocol.event;

import com.nstskj.study.netty.tcp.netty.protocol.event.base.AbstractNetDeviceEvent;
import com.nstskj.study.netty.tcp.netty.protocol.session.base.AbstractSession;
import lombok.Data;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/21 21:11
 * @Description 创建网络设备事件
 */
@Data
public class CreateNetDeviceEvent extends AbstractNetDeviceEvent {
    public CreateNetDeviceEvent(Object source, AbstractSession abstractSession) {
        super(source, abstractSession);
    }
}
