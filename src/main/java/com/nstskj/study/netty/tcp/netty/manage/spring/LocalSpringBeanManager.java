package com.nstskj.study.netty.tcp.netty.manage.spring;

import com.nstskj.study.netty.tcp.netty.config.properties.NettyNetDevTcpServerProperties;
import com.nstskj.study.netty.tcp.netty.protocol.executor.msg.ProtocolHandlerWorkExecutor;
import com.nstskj.study.netty.tcp.netty.protocol.message.in.factory.NetMessageDecoderFactory;
import com.nstskj.study.netty.tcp.netty.protocol.message.out.factory.NetMessageEncoderFactory;
import com.nstskj.study.netty.tcp.netty.protocol.session.factory.SessionIdFactory;
import com.nstskj.study.netty.tcp.netty.protocol.session.manage.NetDeviceSessionManage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/20 20:42
 * @Description spring本地bean管理
 */
@Slf4j
@Data
@Component
public class LocalSpringBeanManager {

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * 服务端配置
     */
    @Autowired
    private NettyNetDevTcpServerProperties nettyNetDevTcpServerProperties;

    /**
     * 报文解析
     */
    @Autowired
    private NetMessageDecoderFactory netMessageDecoderFactory;

    /**
     * 响应编码器工厂
     */
    @Autowired
    private NetMessageEncoderFactory netMessageEncoderFactory;

    /**
     * session id 创建工厂
     */
    @Autowired
    private SessionIdFactory sessionIdFactory;

    /**
     * 设备会话管理器
     * 监听设备注册和关闭消息，并操作
     */
    @Autowired
    private NetDeviceSessionManage netDeviceSessionManage;

    /**
     * 执行器
     */
    @Autowired
    private ProtocolHandlerWorkExecutor protocolHandlerWorkExecutor;

}
