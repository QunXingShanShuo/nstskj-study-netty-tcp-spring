package com.nstskj.study.netty.tcp.common.wrap;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project saas-common-core
 * @date 2020/5/4 12:03
 * @Description 分页包装类工厂
 */
@Slf4j
public abstract class PageVoFactory {

    private PageVoFactory() {
    }

    /**
     * 直接返回空数据
     *
     * @param page
     * @param pageSize
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> PageVo<R> empty(int page, int pageSize) {
        PageVo<R> pageVo = new PageVo<>();
        pageVo.setPageNum(page);
        pageVo.setPageSize(pageSize);
        pageVo.setPageTotal(0);
        pageVo.setTotal(0);
        pageVo.setDataList(Collections.emptyList());
        return pageVo;
    }

    /**
     * 得到bean映射的函数
     *
     * @param clazz 要转换的类型
     * @param <T>   输入类型
     * @param <R>   输出类型
     * @return
     */
    private static <T, R> Function<T, R> getBeanMappingFunction(Class<R> clazz) {
        return obj -> {
            try {
                R newInstance = clazz.newInstance();
                BeanUtil.copyProperties(obj, newInstance);
                return newInstance;
            } catch (Exception e) {
                log.error("bean反射创建实例异常", e);
            }
            return null;
        };
    }

    /**
     * 直接包装  通过反射得到返回值
     *
     * @param iterable
     * @param outClass
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> PageVo<R> originalBuilder(Iterable<T> iterable, Class<R> outClass) {
        return originalBuilder(iterable, getBeanMappingFunction(outClass));
    }

    /**
     * 直接包装  通过隐射得到返回值
     *
     * @param iterable 要包装的数据类型
     * @param <T>      原始类型
     * @param <R>      返回的包装类的泛型
     * @return 包装类
     */
    public static <T, R> PageVo<R> originalBuilder(Iterable<T> iterable, Function<T, R> function) {
        Objects.requireNonNull(function, "映射函数不能为空");
        PageVo<R> pageVo = new PageVo<>();
        if (CollectionUtil.isNotEmpty(iterable)) {
            List<R> list = CollectionUtil.toCollection(iterable)
                    .stream()
                    .map(function)
                    .collect(Collectors.toList());
            pageVo.setPageNum(1);
            pageVo.setPageTotal(1);
            pageVo.setPageSize(list.size());
            pageVo.setTotal(list.size());
            pageVo.setDataList(list);
        }
        return pageVo;
    }


    /**
     * mybatisPlus  通过反射得到返回值
     *
     * @param iPage
     * @param outClass
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> PageVo<R> mybatisPlusBuilder(IPage<T> iPage, Class<R> outClass) {
        return mybatisPlusBuilder(iPage, getBeanMappingFunction(outClass));
    }

    /**
     * mybatisPlus  通过隐射得到返回值
     *
     * @param iPage 要包装的数据类型
     * @param <T>   原始类型
     * @param <R>   返回的包装类的泛型
     * @return 包装类
     */
    public static <T, R> PageVo<R> mybatisPlusBuilder(IPage<T> iPage, Function<T, R> function) {
        Objects.requireNonNull(function, "映射函数不能为空");
        PageVo<R> pageVo = new PageVo<>();
        pageVo.setTotal(iPage.getTotal());
        pageVo.setPageNum(iPage.getCurrent());
        pageVo.setPageSize(iPage.getSize());
        pageVo.setPageTotal(iPage.getPages());
        List<R> list = iPage.getRecords()
                .stream()
                .map(function)
                .collect(Collectors.toList());
        pageVo.setDataList(list);
        return pageVo;
    }


    /**
     * mybatis 分页插件  通过反射得到返回值
     *
     * @param page
     * @param outClass
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> PageVo<R> mybatisBuilder(Page<T> page, Class<R> outClass) {
        return mybatisBuilder(page, getBeanMappingFunction(outClass));
    }

    /**
     * mybatis 分页插件  通过隐射得到返回值
     *
     * @param page 要包装的数据类型
     * @param <T>  原始类型
     * @param <R>  返回的包装类的泛型
     * @return 包装类
     */
    public static <T, R> PageVo<R> mybatisBuilder(Page<T> page, Function<T, R> function) {
        Objects.requireNonNull(function, "映射函数不能为空");
        PageVo<R> pageVo = new PageVo<>();
        //总条数
        pageVo.setTotal(page.getTotal());
        //当前页
        pageVo.setPageNum(page.getPageNum());
        pageVo.setPageSize(page.getPageSize());
        pageVo.setPageTotal(page.getPages());
        List<R> list = page
                .stream()
                .map(function)
                .collect(Collectors.toList());
        pageVo.setDataList(list);
        return pageVo;
    }

}
