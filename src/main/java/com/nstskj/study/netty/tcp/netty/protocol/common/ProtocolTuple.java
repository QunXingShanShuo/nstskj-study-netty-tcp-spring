package com.nstskj.study.netty.tcp.netty.protocol.common;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/5/4 18:41
 * @Description 元组
 */
@Slf4j
@Data
public class ProtocolTuple<one, two> implements Serializable {

    private final one one;
    private final two two;

    public ProtocolTuple(one one, two two) {
        this.one = one;
        this.two = two;
    }
}
