package com.nstskj.study.netty.tcp.netty.protocol.endpoint;

import com.nstskj.study.netty.tcp.netty.protocol.endpoint.base.AbstractEndpoint;
import lombok.Data;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp
 * @date 2021/4/19 14:18
 * @Description 设备端点
 */
@Data
public class DeviceEndpoint extends AbstractEndpoint {

    private static final long serialVersionUID = 1L;

    /**
     * 构造
     *
     * @param address
     * @param isOut
     * @param buffLen
     */
    public DeviceEndpoint(short address, boolean isOut, int buffLen) {
        super(address, isOut, buffLen);
    }
}
