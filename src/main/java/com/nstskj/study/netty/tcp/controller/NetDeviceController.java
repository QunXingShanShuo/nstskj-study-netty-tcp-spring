package com.nstskj.study.netty.tcp.controller;

import com.nstskj.study.netty.tcp.common.base.Result;
import com.nstskj.study.netty.tcp.common.vo.GetAddressDataBySessionIdVO;
import com.nstskj.study.netty.tcp.common.vo.NetDeviceStatusVO;
import com.nstskj.study.netty.tcp.common.vo.NetDeviceVO;
import com.nstskj.study.netty.tcp.common.wrap.PageVo;
import com.nstskj.study.netty.tcp.netty.protocol.endpoint.data.EndpointData;
import com.nstskj.study.netty.tcp.service.NetDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-study-netty-tcp-spring
 * @date 2021/4/22 21:17
 * @Description 网络设备控制器
 */
@Slf4j
@Api(value = "网络设备管理", tags = "网络设备管理")
@RequestMapping("/v1/netDevice/1.0")
@RestController
public class NetDeviceController {

    @Autowired
    private NetDeviceService netDeviceService;

    /**
     * @return
     */
    @ApiOperation("得到设备信息")
    @GetMapping("/getNetDeviceList")
    public Result<PageVo<NetDeviceVO>> getNetDeviceList() {
        PageVo<NetDeviceVO> netDeviceVOPageVo = netDeviceService.getNetDeviceList();
        return Result.ok(netDeviceVOPageVo);
    }

    @ApiImplicitParam(name = "sessionId", value = "设备会话Id")
    @ApiOperation("关闭指定的设备")
    @GetMapping("/closeNetDeviceBySessionId")
    public Result<NetDeviceStatusVO> closeNetDeviceBySessionId(@RequestParam("sessionId") String sessionId) {
        boolean aBoolean = netDeviceService.closeNetDeviceBySessionId(sessionId);
        NetDeviceStatusVO netDeviceStatusVO = new NetDeviceStatusVO();
        netDeviceStatusVO.setStatus(aBoolean);
        return Result.ok(netDeviceStatusVO);
    }

    @ApiOperation("向指定设备的端点发送数据")
    @GetMapping("/sendAddressDataBySessionId")
    public Result<NetDeviceStatusVO> sendAddressDataBySessionId(@RequestParam("sessionId") String sessionId, @RequestParam("address") Short address, @RequestParam("data") Short data) {
        boolean aBoolean = netDeviceService.sendAddressDataBySessionId(sessionId, address, data);
        NetDeviceStatusVO netDeviceStatusVO = new NetDeviceStatusVO();
        netDeviceStatusVO.setStatus(aBoolean);
        return Result.ok(netDeviceStatusVO);
    }

    @ApiOperation("读取指定设备的端点数据")
    @GetMapping("/getAddressDataBySessionId")
    public Result<GetAddressDataBySessionIdVO> getAddressDataBySessionId(@RequestParam("sessionId") String sessionId, @RequestParam("address") Short address) {
        List<EndpointData> endpointDataList = netDeviceService.getAddressDataBySessionId(sessionId, address);
        GetAddressDataBySessionIdVO getAddressDataBySessionIdVO = new GetAddressDataBySessionIdVO();
        getAddressDataBySessionIdVO.setEndpointDataList(endpointDataList);
        return Result.ok(getAddressDataBySessionIdVO);
    }

}
